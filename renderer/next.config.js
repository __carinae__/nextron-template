const aliasWebpackConfig = config => {
  config.target = 'electron-renderer';
  config.resolve.alias = {
    ...config.resolve.alias,
    '@@package': './package.json',
    '@@core': './renderer/core',
    '@@identifier': './renderer/core/__identifier__',
    '@@interface': './renderer/core/__interface__',
    '@@pageComponents': './renderer/pageComponents',
    '@@defaultData': './renderer/defaultData',
  };
  // module: {
  //   rules: [
  //     {
  //       test: /\.css$/,
  //       use: ['style-loader', 'css-loader'],
  //     },
  //   ],
  // },
  return config;
};

module.exports = {
  webpack: config => aliasWebpackConfig(config),
};
