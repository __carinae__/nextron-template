import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

const drawerWidth = 50;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      textAlign: 'center',
      '& .MuiBadge-root': {
        marginRight: theme.spacing(4),
      },
      display: 'flex',
      flexGrow: 1,
    },
    drawer: {
      width: drawerWidth,
    },
    drawerPaper: {
      width: drawerWidth,
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
      borderRight: `1px solid ${theme.palette.background.default}`,
    },
    drawerPaperRight: {
      width: drawerWidth,
      overflow: 'hidden',
      backgroundColor: theme.palette.background.default,
      borderLeft: `1px solid ${theme.palette.background.default}`,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-end',
    },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      minHeight: '100vh',
    },
    tab: {
      minWidth: drawerWidth,
      width: drawerWidth,
    },
    tabs: {
      // borderRight: `1px solid ${theme.palette.divider}`,
    },
    tooltip: {
      fontSize: 10,
      fontWeight: theme.typography.fontWeightBold,
    },
    tabTooltip: {
      fontSize: 10,
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.background.default,
      fontWeight: theme.typography.fontWeightBold,
    },
    tabTooltipUnselected: {
      fontSize: 10,
      // backgroundColor: theme.palette.secondary.main,
      // color: theme.palette.background.default,
      fontWeight: theme.typography.fontWeightBold,
    },
    tabTooltipArrow: {
      color: theme.palette.secondary.main,
    },
  })
);
