export const centerize = `
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
`;

export const color = { primary: '#9caeff', secondary: '#c7c9f9' };
