import styled, { keyframes } from 'styled-components';
import { color, centerize } from './shared';

const size = { rhombus: '15px', ball: '9px' };
const delay = '700ms';

export const LoopingRhombusesSpinner = styled.div`
  box-sizing: border-box;
  & *: {
    box-sizing: border-box;
  }
  width: calc(${size.rhombus} * 4);
  height: ${size.rhombus};
  ${centerize}
`;

const LoopingRhombusesSpinnerAnimation = keyframes`
  0% {
    transform: translateX(0) rotate(45deg) scale(0);
    background-color: ${color.primary};
  }
  50% {
    transform: translateX(calc(-${size.rhombus} * 2)) rotate(45deg) scale(1);
    background-color: ${color.secondary};
  }
  100% {
    transform: translateX(calc(-${size.rhombus} * 4)) rotate(45deg) scale(0);
    background-color: ${color.primary};
  }
`;

export const Rhombus = styled.div`
  height: ${size.rhombus};
  width: ${size.rhombus};
  left: calc(${size.rhombus} * 4 - ${size.rhombus} / 2);
  position: absolute;
  margin: 0 auto;
  border-radius: 2px;
  transform-origin: center;
  transform: translateY(0) rotate(45deg) scale(0);
  animation: ${LoopingRhombusesSpinnerAnimation} calc(${delay} * 5) linear infinite;
  :nth-child(1) {
    animation-delay: calc(-${delay} * 1);
  }
  :nth-child(2) {
    animation-delay: calc(-${delay} * 2);
  }
  :nth-child(3) {
    animation-delay: calc(-${delay} * 3);
  }
  :nth-child(4) {
    animation-delay: calc(-${delay} * 4);
  }
  :nth-child(5) {
    animation-delay: calc(-${delay} * 5);
  }
`;

const ballColor = color.secondary;

const LoopingBoundBallAnimation = keyframes`
  0% {
    transform: translate(calc(-${size.rhombus} * 2), calc(${size.rhombus} / 2)) rotate(45deg);
  }
  25% {
    transform: translate(calc(-${size.rhombus} * 2), calc(${size.rhombus} / 4)) rotate(90deg);
  }
  50% {
    transform: translate(calc(-${size.rhombus} * 2), 0) rotate(135deg);
  }
  75% {
    transform: translate(calc(-${size.rhombus} * 2), calc(${size.rhombus} / 4)) rotate(180deg);
  }
  100% {
    transform: translate(calc(-${size.rhombus} * 2), calc(${size.rhombus} / 2)) rotate(225deg);
  }
`;

export const BoundBall = styled.div`
  position: absolute;
  bottom: calc(${size.rhombus} + ${size.ball} / 2 - 1px);
  left: calc(${size.rhombus} * 4 - ${size.ball} / 2);
  width: ${size.ball};
  border: 4px solid ${ballColor};
  background-color: #e7e9ff;
  height: ${size.ball};
  border-radius: 2px;
  transform-origin: center;
  animation: ${LoopingBoundBallAnimation} ${delay} linear infinite;
`;
