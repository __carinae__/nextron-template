import styled, { keyframes } from 'styled-components';
import { color, centerize } from './shared';

export const OrbitSpinner = styled.div`
  box-sizing: border-box;
  & *: {
    box-sizing: border-box;
  }
  height: 80px;
  width: 80px;
  border-radius: 50%;
  perspective: 800px;
  ${centerize}
`;

const OrbitSpinnerAnimationOne = keyframes`
  0% {
    transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
  }
  100% {
    transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
  }
`;

const OrbitSpinnerAnimationTwo = keyframes`
  0% {
    transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
  }
  100% {
    transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
  }
`;

const OrbitSpinnerAnimationThree = keyframes`
  0% {
    transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
  }
  100% {
    transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
  }
`;

export const Orbit = styled.div`
  position: absolute;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  :nth-child(1) {
    left: 0%;
    top: 0%;
    animation: ${OrbitSpinnerAnimationOne} 1200ms linear infinite;
    border-bottom: 3px solid ${color.secondary};
  }
  :nth-child(2) {
    right: 0%;
    top: 0%;
    animation: ${OrbitSpinnerAnimationTwo} 1200ms linear infinite;
    border-right: 3px solid ${color.secondary};
  }
  :nth-child(3) {
    right: 0%;
    bottom: 0%;
    animation: ${OrbitSpinnerAnimationThree} 1200ms linear infinite;
    border-top: 3px solid ${color.secondary};
  }
`;
