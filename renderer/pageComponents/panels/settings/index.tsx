import React from 'react';
import { Typography } from '@material-ui/core';
import { Settings } from '@material-ui/icons';
import { useStyles } from '@@pageComponents/style/home';
import { LoopingRhombusesSpinner, Rhombus, BoundBall } from '@@pageComponents/style/loading/loopingRhombus';

const Panel: React.FC = () => {
  const classes = useStyles({});

  return (
    <>
      <Typography variant="h5" style={{ fontWeight: 600 }} gutterBottom>
        Shared Settings
      </Typography>
      <div className={classes.root}>
        <LoopingRhombusesSpinner>
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <BoundBall />
        </LoopingRhombusesSpinner>
      </div>
    </>
  );
};

export default {
  name: 'Settings',
  component: Panel,
  icon: Settings,
  help: null,
};
