import * as first from './first';
import * as second from './second';
import * as settings from './settings';

/**
 * Register panels.
 * */
const __panels = [first, second, settings];

export const panelKeys = Array.from({ length: __panels.length }, (_, i) => 'no-' + i);
export const panels = Object.fromEntries(panelKeys.map((key, i) => [key, __panels[i].default]));
