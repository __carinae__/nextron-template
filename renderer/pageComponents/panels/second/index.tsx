import React from 'react';
import { Button, Typography } from '@material-ui/core';
import { Note } from '@material-ui/icons';
// import { useStyles } from '@@pageComponents/style/home';
import HelpDialog from './help';

const Panel: React.FC = () => {
  // const classes = useStyles({});

  return (
    <>
      <Typography variant="h5" style={{ fontWeight: 600 }} gutterBottom>
        Desktop SPA Template
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        with Nextron / Redux / Material-UI
      </Typography>
      <img src="/images/logo.png" />
      <br />
      <Button variant="contained" color="primary">
        Unreactable button
      </Button>
    </>
  );
};

export default {
  name: 'Note',
  component: Panel,
  icon: Note,
  help: HelpDialog,
};
