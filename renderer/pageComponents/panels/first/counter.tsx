import React from 'react';
import { Button, Badge, ButtonGroup, Box } from '@material-ui/core';
import { Add, Remove, Refresh, Mail, Publish, Delete } from '@material-ui/icons';
import { useDispatch } from 'react-redux';
import { useSelector, selectors, actions, load } from '@@core/redux';
// import { useStyles } from '@@pageComponents/style/home';

const CounterComponent: React.FC = () => {
  // const classes = useStyles({});
  const dispatch = useDispatch();
  const sortFilterProps = useSelector(state => state.counterPanel.sortFilterProps.counters);
  const counters = useSelector(state => {
    return selectors.counter.counters(state, sortFilterProps);
  });

  return (
    <>
      <Box style={{ padding: 4 }}>
        <Button aria-label="add" onClick={() => dispatch(actions.counter.appendCounter({}))}>
          <Add fontSize="default" />
        </Button>
        <Button aria-label="load" onClick={() => load(dispatch)}>
          <Publish fontSize="default" />
        </Button>
      </Box>
      <Box style={{ padding: 2 }}>
        {counters.map(counter => {
          const { id, count } = counter;
          return (
            <Box key={'counter:' + id} style={{ padding: 2 }}>
              <Badge color="secondary" badgeContent={count}>
                <Mail fontSize="small" />
              </Badge>
              <ButtonGroup>
                <Button
                  aria-label="reduce"
                  onClick={() => {
                    dispatch(actions.counter.decrement({ id, diff: 1 }));
                  }}>
                  <Remove fontSize="small" />
                </Button>
                <Button
                  aria-label="increase"
                  onClick={() => {
                    dispatch(actions.counter.increment({ id, diff: 1 }));
                  }}>
                  <Add fontSize="small" />
                </Button>
                <Button
                  aria-label="reset"
                  onClick={() => {
                    dispatch(actions.counter.setCounter({ id, changes: { count: 0 } }));
                  }}>
                  <Refresh fontSize="small" />
                </Button>
                <Button
                  aria-label="delete"
                  onClick={() => {
                    dispatch(actions.counter.deleteCounter(id));
                  }}>
                  <Delete fontSize="small" />
                </Button>
              </ButtonGroup>
            </Box>
          );
        })}
      </Box>
    </>
  );
};

export default CounterComponent;
