import React from 'react';
import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useSelector, actions } from '@@core/redux';

// import { useStyles } from '../../style/home';

const msg = [...new Array(50)]
  .map(
    () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
  )
  .join('\n');

const HelpDialog: React.FC = () => {
  // const classes = useStyles({});
  const dispatch = useDispatch();
  const { open } = useSelector(state => state.help);

  const handleClose = () => dispatch(actions.help.setOpen(false));

  const descriptionElementRef = React.useRef<HTMLElement>(null);

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description">
        <DialogTitle id="scroll-dialog-title">Help - Mail</DialogTitle>
        <DialogContent dividers>
          <DialogContentText id="scroll-dialog-description" ref={descriptionElementRef} tabIndex={-1} align="justify">
            {msg}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default HelpDialog;
