import React from 'react';
import { Typography } from '@material-ui/core';
import { Mail } from '@material-ui/icons';
// import { useStyles } from '@@pageComponents/style/home';
import HelpDialog from './help';
import CounterComponent from './counter';

const Pannel: React.FC = () => {
  // const classes = useStyles({});

  return (
    <>
      <Typography variant="h5" style={{ fontWeight: 600 }} gutterBottom>
        Desktop SPA Template
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        with Nextron / Redux / Material-UI
      </Typography>
      <img src="/images/logo.png" />
      <br />
      <CounterComponent />
    </>
  );
};

export default {
  name: 'Mail',
  component: Pannel,
  icon: Mail,
  help: HelpDialog,
};
