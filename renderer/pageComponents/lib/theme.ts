import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#8c9eff',
    },
    secondary: {
      main: '#c7c9f9',
    },
    background: {
      default: '#202022',
      paper: '#282829',
    },
  },
  typography: {
    /**
     * In Japanese the characters are usually larger.
     * The computed font size F by the browser follows this mathematical equation:
     * F = {specification} * (typography.fontsize}/14 * {html.fontsize}/{typography.htmlFontsize}}
     */
    fontSize: 11,
    /**
     * Tell Material-UI what's the font-size on the html element is.
     * font-size: 62.5%; // 62.5% of 16px = 10px
     */
    htmlFontSize: 16,
  },
});
