import { PickType, DefaultDataSchemas } from '@@interface';
import * as repository from '@@core/repository';
import * as useCase from '@@core/useCase';
import * as adapters from '@@core/adapters';
import { name, createSliceFunc, defaultData, getCustomSelectors, getCustomExtractors } from '@@core/redux/core/counter';

const gateway = new repository[name].Gateway(defaultData as PickType<DefaultDataSchemas, typeof name>, name);
const interactor = new useCase[name].Interactor(gateway);
const adapterRecord = adapters[name].adapterRecord;

export const slice = createSliceFunc(interactor, adapterRecord);
export { name, getCustomSelectors, getCustomExtractors };
