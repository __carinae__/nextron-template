import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';
import { name } from '@@identifier/ui/help';

interface StateSchema {
  open: boolean;
}

export const initialState: StateSchema = {
  open: false,
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers: {
    setOpen: (state: StateSchema, action: PayloadAction<boolean>) => {
      state.open = action.payload;
    },
  },
});

export { name, slice };
