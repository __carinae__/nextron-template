import { createSlice, Slice } from '@reduxjs/toolkit';
import { name } from '@@identifier/ui/counter';
import { SortFilterProps } from '@@interface';

type StateSchema = {
  sortFilterProps: { counters: SortFilterProps };
};

export const initialState: StateSchema = {
  sortFilterProps: {
    counters: {
      sortKey: ['id'],
      sortOrder: 'asc' as const,
      filters: [],
    },
  },
};

const slice: Slice = createSlice({
  name,
  initialState,
  reducers: {},
});

export { name, slice };
