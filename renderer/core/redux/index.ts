import { useSelector as rawUseSelector, TypedUseSelectorHook } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from '@reduxjs/toolkit';
import * as uiModules from './ui';
import * as coreModules from './dependencyInjection';

/**
 * Define a root reducer module of redux.
 */
const rootReducer = combineReducers({
  ...Object.fromEntries(Object.values(coreModules).map(__module => [__module.name, __module.slice.reducer])),
  ...Object.fromEntries(Object.values(uiModules).map(__module => [__module.name, __module.slice.reducer])),
});
type RootState = ReturnType<typeof rootReducer>;

/**
 * Define a dictionary of selector groups which can be invoked by a redux module name.
 */
export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;
export const useSelectorWithInitialValue = <T>(func: (state: RootState) => T, init: T): T => {
  return rawUseSelector(func) ?? init;
};

export const selectors = {
  counter: coreModules.counter.getCustomSelectors<RootState>(),
};

export const extractors = {
  counter: coreModules.counter.getCustomExtractors<RootState>(),
};

/**
 * Define a dictionary of action groups which can be invoked by a redux module name.
 */
export const actions = {
  ...Object.fromEntries(Object.values(coreModules).map(__module => [__module.name, __module.slice.actions])),
  ...Object.fromEntries(Object.values(uiModules).map(__module => [__module.name, __module.slice.actions])),
};

/**
 * Define a redux store.
 */
const store = configureStore({ reducer: rootReducer });
export default store;
export type AppDispatch = typeof store.dispatch;

/**
 * Load all the states.
 */
export const load = (dispatch: AppDispatch): void => {
  Object.values(coreModules).forEach(__module => {
    const { slice } = __module;
    if ('load' in slice.actions) {
      dispatch(slice.actions.load({}));
    }
  });
};

/**
 * Save all the states.
 */
export const save = (dispatch: AppDispatch): void => {
  Object.values(coreModules).forEach(__module => {
    const { slice } = __module;
    if ('save' in slice.actions) {
      dispatch(slice.actions.save({}));
    }
  });
};
