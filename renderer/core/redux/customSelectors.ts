import { OutputParametricSelector, createSelector } from '@reduxjs/toolkit';
import { SortFilterProps, SelectorPayload } from '@@interface';
import { Identifiers } from '@@identifier/core';
import * as adapters from '@@core/adapters';
import { deepAccess, compareVars } from '@@core/utils';

/**
 * Normalize input string for
 * 1) equating Hira-ganas and Kata-kanas,
 * 2) equating full-width and half-width alphabets and numbers,
 * 3) ignoring spaces on both ends.
 */
const normalizeString = (str: string): string => {
  if (!str) {
    return str;
  }
  return str
    .trim()
    .replace(/[ぁ-ん]/g, b => String.fromCharCode(b.charCodeAt(0) + 0x60))
    .replace(/[Ａ-Ｚａ-ｚ０-９]/g, b => String.fromCharCode(b.charCodeAt(0) - 0xfee0));
};

const sortFilterEntities = (payload: SelectorPayload<({ id: string } & Record<string, unknown>)[]>) => {
  const { entities, sortKey, sortOrder, filters }: SelectorPayload<({ id: string } & Record<string, unknown>)[]> = {
    sortKey: ['id'],
    sortOrder: 'asc',
    filters: [],
    ...payload,
  };

  // Sort all the entities by a given key and order.
  let res = entities.slice().sort((a, b) => {
    const [ak, bk] = [deepAccess(a, sortKey).toString(), deepAccess(b, sortKey).toString()];
    let res: number;
    if (sortOrder === 'asc') {
      res = ak.localeCompare(bk, 'ja', { numeric: true });
    } else {
      res = bk.localeCompare(ak, 'ja', { numeric: true });
    }
    if (res === 0 && !compareVars(sortKey, ['id'])) {
      return a.id.localeCompare(b.id, 'ja', { numeric: true });
    } else {
      return res;
    }
  });

  // Filter by given keys and queries with ambiguous matching.
  filters.forEach(filter => {
    const { key, queries } = filter;
    if (key !== null) {
      res = res.filter(a => {
        const ak = deepAccess(a, key);
        return queries.some(query => {
          if (typeof query == 'number') {
            return ak === query;
          } else if (typeof query == 'string') {
            return normalizeString(ak.toString()) === normalizeString(query);
          } else {
            return query(ak);
          }
        });
      });
    }
  });

  return res;
};

export function getSelectors<
  RootState,
  Keys extends string,
  SelectorsType extends {
    [K in Keys]: OutputParametricSelector<
      RootState,
      SortFilterProps,
      unknown[],
      (res: SelectorPayload<unknown[]>) => unknown[]
    >;
  }
>(name: keyof Identifiers): SelectorsType {
  const adapterRecord = adapters[name].adapterRecord;
  return Object.fromEntries(
    Object.keys(adapterRecord).map(key => {
      const adapter = adapterRecord[key];
      const selector = adapter.getSelectors((state: RootState) => state[name][key]);
      return [
        key,
        createSelector((state: RootState, props: SortFilterProps) => {
          return { entities: selector.selectAll(state), ...props };
        }, sortFilterEntities),
      ];
    })
  ) as SelectorsType;
}

export function getExtractors<
  RootState,
  Keys extends string,
  SelectorsType extends {
    [K in Keys]: OutputParametricSelector<
      RootState,
      SortFilterProps,
      unknown[],
      (res: SelectorPayload<unknown[]>) => unknown[]
    >;
  }
>(name: keyof Identifiers): SelectorsType {
  const adapterRecord = adapters[name].adapterRecord;
  return Object.fromEntries(
    Object.keys(adapterRecord).map(key => {
      const adapter = adapterRecord[key];
      const selector = adapter.getSelectors((state: RootState) => state[name][key]);
      return [
        key,
        createSelector((state: RootState, props: SortFilterProps) => {
          let memoProps = ['id'];
          if (props.sortKey != null) {
            memoProps.push(props.sortKey[0]);
          }
          props.filters.forEach(filter => {
            memoProps.push(filter.key[0]);
          });
          memoProps = Array.from(new Set(memoProps));
          return {
            entities: selector
              .selectAll(state)
              .map(entity => Object.fromEntries(memoProps.map(prop => [prop, entity[prop]]))),
            ...props,
          };
        }, sortFilterEntities),
      ];
    })
  ) as SelectorsType;
}
