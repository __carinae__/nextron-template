import { createSlice, PayloadAction, Slice, OutputParametricSelector } from '@reduxjs/toolkit';
import {
  PickType,
  AdapterRecords,
  IEntityRecords,
  UseCases,
  StateSchemas,
  SortFilterProps,
  SelectorPayload,
  ICounter,
} from '@@interface';
import { getSelectors, getExtractors } from '../customSelectors';
import { name } from '@@identifier/core/counter';
import defaultData from '@@defaultData/counter.default.json';

type StateSchema = PickType<StateSchemas, typeof name>;

/**
 * Create a reducer slice with initial states reflecting the newest database when launching the app.
 * Calling laod function in the home routed page with useEffect is unsufficient
 * because page contents will be reset to the database state after page transition.
 */
export const createSliceFunc = (
  interactor: PickType<UseCases, typeof name>,
  adapters: PickType<AdapterRecords, typeof name>
): Slice =>
  createSlice({
    name,
    initialState: interactor.load(adapters),
    /**
     * Set reducer actions.
     */
    reducers: {
      appendCounter: (state: StateSchema) => {
        adapters.counters.addOne(
          state.counters,
          interactor.getInitialCounter(String(Math.max(...state.counters.ids.map(id => Number(id))) + 1))
        );
      },

      deleteCounter: (state: StateSchema, action: PayloadAction<number>) => {
        adapters.counters.removeOne(state.counters, action.payload);
      },

      setCounter: (state: StateSchema, action: PayloadAction<{ id: string; changes: Partial<ICounter> }>) => {
        const { id, changes } = action.payload;
        adapters.counters.updateOne(state.counters, { id, changes });
      },

      increment: (state: StateSchema, action: PayloadAction<{ id: number; diff: number }>) => {
        const { id, diff } = action.payload;
        adapters.counters.updateOne(state.counters, {
          id,
          changes: interactor.increment(state.counters.entities[id], diff),
        });
      },

      decrement: (state: StateSchema, action: PayloadAction<{ id: number; diff: number }>) => {
        const { id, diff } = action.payload;
        adapters.counters.updateOne(state.counters, {
          id,
          changes: interactor.decrement(state.counters.entities[id], diff),
        });
      },

      load: (state: StateSchema) => {
        return { ...state, ...interactor.load(adapters) };
      },

      save: (state: StateSchema) => {
        interactor.save(state);
      },
    },
  });

/**
 * Type of record which contains custom selectors related to the identifier.
 */
type SelectorsType<RootState> = {
  [K in keyof PickType<AdapterRecords, typeof name>]: OutputParametricSelector<
    RootState,
    SortFilterProps,
    PickType<IEntityRecords, typeof name>[K],
    (res: SelectorPayload<PickType<IEntityRecords, typeof name>[K]>) => PickType<IEntityRecords, typeof name>[K]
  >;
};

/**
 * Get a record of custom selectors related to the identifier.
 */
export const getCustomSelectors = <RootState>(): SelectorsType<RootState> =>
  getSelectors<RootState, keyof PickType<AdapterRecords, typeof name>, SelectorsType<RootState>>(name);

/**
 * Get a record of custom extractors related to the identifier.
 */
export const getCustomExtractors = <RootState>(): SelectorsType<RootState> =>
  getExtractors<RootState, keyof PickType<AdapterRecords, typeof name>, SelectorsType<RootState>>(name);

export { name, defaultData };
