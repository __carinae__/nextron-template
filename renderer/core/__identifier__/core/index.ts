import * as counter from './counter';

export interface Identifiers {
  [counter.name]: typeof counter.name;
}
