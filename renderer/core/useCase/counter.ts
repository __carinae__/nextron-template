import { PickType, ICounter, UseCases, AdapterRecords, StateSchemas } from '@@interface';
import { InteractorBase } from './interactor';
import { name } from '@@identifier/core/counter';
import * as domains from '@@core/domain';

const domain = domains[name];

/**
 * The use case class of counter objects.
 */
export class Interactor
  extends InteractorBase<PickType<AdapterRecords, typeof name>, PickType<StateSchemas, typeof name>>
  implements PickType<UseCases, typeof name>
{
  /**
   * Return an initialized counter object with a given id.
   */
  getInitialCounter(id: string): ICounter {
    return domain.getInitialCounter(id);
  }

  /**
   * Add a number to a given counter's count.
   */
  increment(counter: ICounter, diff: number): ICounter {
    return domain.increment(counter, diff);
  }

  /**
   * Subtract a number from a given counter's count.
   */
  decrement(counter: ICounter, diff: number): ICounter {
    return domain.decrement(counter, diff);
  }
}
