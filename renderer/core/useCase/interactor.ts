import { UseCaseBase, Repository } from '@@interface';

/**
 * Implementation of use case class.
 */
export class InteractorBase<AdapterRecord, StateSchema> implements UseCaseBase<AdapterRecord, StateSchema> {
  private repository: Repository<AdapterRecord, StateSchema>;

  /**
   * Set a repository.
   */
  constructor(repository: Repository<AdapterRecord, StateSchema>) {
    this.repository = repository;
  }

  /**
   * Load saved data if exists.
   */
  load(adapters: AdapterRecord): StateSchema {
    return this.repository.load(adapters);
  }

  /**
   * Save given data to the database.
   */
  save(state: StateSchema): void {
    this.repository.save(state);
  }
}
