import merge from 'deepmerge';

export const deepAccess = (data: Record<string, unknown>, keys: string[]): unknown => {
  return keys.reduce((current, key) => {
    try {
      return current[key];
    } catch (e) {
      return undefined;
    }
  }, data);
};

type SomeVar = string | number | unknown[] | Record<string, unknown>;
export const compareVars = (a: SomeVar, b: SomeVar): boolean => {
  return JSON.stringify(a) === JSON.stringify(b);
};

export const arrayOverrideMerge = <T>(a: T, b: T): T => {
  const overwriteMerge = (destinationArray: unknown[], sourceArray: unknown[]): unknown[] => sourceArray;
  return merge<T>(a, b, { arrayMerge: overwriteMerge });
};
