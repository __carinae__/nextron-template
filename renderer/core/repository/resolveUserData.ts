import * as path from 'path';
import * as os from 'os';
import pack from '@@package';

const platform = os.platform();

const appName = pack.productName;
let userData = '';

if (platform === 'win32') {
  userData = path.join(process.env.APPDATA, appName);
} else if (platform === 'darwin') {
  userData = path.join(process.env.HOME, 'Library', 'Application Support', appName);
} else {
  userData = path.join('var', 'local', appName);
}

export const userDatabase = path.join(userData, 'Database');
