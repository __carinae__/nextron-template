import { GatewayBase } from './gateway';
import { PickType, AdapterRecords, StateSchemas, DataStoreSchemas, DefaultDataSchemas } from '@@interface';
import { name } from '@@identifier/core/counter';

type AdapterRecord = PickType<AdapterRecords, typeof name>;
type StateSchema = PickType<StateSchemas, typeof name>;
type DefaultDataSchema = PickType<DefaultDataSchemas, typeof name>;
type DataStoreSchema = PickType<DataStoreSchemas, typeof name>;

/**
 * Gateway for counter data.
 */
export class Gateway extends GatewayBase<AdapterRecord, StateSchema, DefaultDataSchema, DataStoreSchema> {
  /**
   * Convert user saved data if it is outdated and has conflict with the latest data schema.
   */
  convertOutdatedDataFormat(data: DataStoreSchema): DataStoreSchema {
    return data;
  }
}
