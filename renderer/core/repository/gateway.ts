import * as path from 'path';
import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig';
import { Repository, DefaultDataSchemaFrame, DataStoreSchemaFrame, StateSchemaFrame } from '@@interface';
import { userDatabase } from './resolveUserData';
import { arrayOverrideMerge } from '@@core/utils';
import { version } from '@@package';

const sep = '/';
const fileExt = '.db.json';

/**
 * Gateway for saving and loading data.
 */
export class GatewayBase<
  AdapterRecord,
  StateSchema extends StateSchemaFrame,
  DefaultDataSchema extends DefaultDataSchemaFrame,
  DataStoreSchema extends DataStoreSchemaFrame
> implements Repository<AdapterRecord, StateSchema>
{
  private dbPath: string;
  private db: JsonDB;
  dbFilename: string;
  keys: string[];

  /**
   * Define a json database.
   */
  constructor(defaultData: DefaultDataSchema, name: string) {
    this.dbFilename = name + fileExt;
    this.dbPath = path.join(userDatabase, this.dbFilename);
    this.db = new JsonDB(new Config(this.dbPath, true, true, sep));
    this.keys = Object.keys(defaultData.basis);

    const emptyData = { memory: {}, version };
    if (!this.db.exists(sep)) {
      // Set default values if database is empty.
      this.db.push(sep, this.dataMerge(defaultData, emptyData as DataStoreSchema));
    } else {
      // Merge default values and saved data if exists.
      // Data may be overriden if needed.
      this.db.push(sep, this.dataMerge(defaultData, { ...emptyData, ...this.db.getObject<DataStoreSchema>(sep) }));
    }
  }

  /**
   * Merge the default data and user saved data.
   * This function must be able to properly deal with the data conflict caused by version updates.
   */
  dataMerge(defaultData: DefaultDataSchema, data: DataStoreSchema): DataStoreSchema {
    data = this.convertOutdatedDataFormat(data);
    const dataVersion = this.splitVersionInfo(data.version);
    let mergedData = arrayOverrideMerge(defaultData.basis, data.memory);

    const overrideVersions = Object.keys(defaultData.override).map(this.splitVersionInfo).sort(this.compareVersions);
    overrideVersions.forEach(overrideVersion => {
      if (this.compareVersions(overrideVersion, dataVersion) === 1) {
        mergedData = arrayOverrideMerge(mergedData, defaultData.override[overrideVersion.join('.')]);
      }
    });

    return { memory: mergedData, version } as DataStoreSchema;
  }

  /**
   * Convert user saved data if it is outdated and has conflict with the latest data schema.
   * Expected to be implemented with switch sentences.
   * [NOTE] Expected to be overiden if needed.
   */
  convertOutdatedDataFormat(data: DataStoreSchema): DataStoreSchema {
    return data;
  }

  /**
   * Extract version information as numbers from string version tag.
   * e.g., '0.0.1' => [0, 0, 1]
   */
  splitVersionInfo(version: string): number[] {
    return version.split('.').map(str => Number(str));
  }

  /**
   * Return a number from [-1, 0, 1] which can be used in a sort function.
   */
  compareVersions(a: number[], b: number[]): number {
    let c = 0;
    a.forEach((num, i) => {
      if (num < b[i]) {
        c = -1;
      } else if (num > b[i]) {
        c = 1;
      }
    });
    return c;
  }

  /**
   * Load saved data if exists.
   */
  load(adapters: AdapterRecord): StateSchema {
    this.db.reload();
    const data = this.db.getObject<DataStoreSchema>(sep).memory;

    return Object.fromEntries(
      this.keys.map(key => {
        const d = data[key];
        if (key in adapters) {
          return [key, { ids: Object.keys(d), entities: d }];
        } else {
          return [key, d];
        }
      })
    ) as StateSchema;
  }

  /**
   * Save given data to the database.
   */
  save(state: StateSchema): void {
    this.db.push(sep, {
      memory: Object.fromEntries(
        this.keys.map(key => {
          const s = state[key];
          if (s.hasOwnProperty('entities')) {
            return [key, s['entities']];
          } else {
            return [key, s];
          }
        })
      ),
      version,
    });
  }
}
