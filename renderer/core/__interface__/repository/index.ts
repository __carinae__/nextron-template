import { IEntityHashRecords } from '@@interface/adapters';

type DataStoreSchemaProps = {
  version: string;
};

export type DefaultDataSchemaFrame = {
  basis: Record<string, unknown>;
  override: Record<string, Record<string, unknown>>;
};

export type DefaultDataSchemas = {
  [K in keyof IEntityHashRecords]: {
    basis: IEntityHashRecords[K] & Record<string, unknown>;
    override: { [version: string]: Partial<IEntityHashRecords[K] & Record<string, unknown>> };
  };
};

export type DataStoreSchemaFrame = { memory: Record<string, unknown> } & DataStoreSchemaProps;

export type DataStoreSchemas = {
  [K in keyof IEntityHashRecords]: { memory: IEntityHashRecords[K] & Record<string, unknown> } & DataStoreSchemaProps;
};

export interface Repository<AdapterRecord, StateSchema> {
  load: (adapters: AdapterRecord) => StateSchema;
  save: (state: StateSchema) => void;
}
