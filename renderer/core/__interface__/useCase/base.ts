export interface UseCaseBase<AdapterRecord, StateSchema> {
  load: (adapters: AdapterRecord) => StateSchema;
  save: (state: StateSchema) => void;
}
