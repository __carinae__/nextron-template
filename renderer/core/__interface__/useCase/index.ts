import * as counter from './counter';
export type { UseCaseBase } from './base';

export type UseCases = {
  counter: counter.UseCase;
};
