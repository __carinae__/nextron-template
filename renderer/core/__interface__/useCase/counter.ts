import { ICounter } from '@@interface/domain';
import { AdapterRecords } from '@@interface/adapters';
import { StateSchemas } from '@@interface/redux';
import { PickType } from '../shared';
import { UseCaseBase } from './base';
import { name } from '@@identifier/core/counter';

export interface UseCase
  extends UseCaseBase<PickType<AdapterRecords, typeof name>, PickType<StateSchemas, typeof name>> {
  getInitialCounter: (id: string) => ICounter;
  increment: (counter: ICounter, diff: number) => ICounter;
  decrement: (counter: ICounter, diff: number) => ICounter;
}
