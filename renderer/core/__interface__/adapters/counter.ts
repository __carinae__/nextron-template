import { ICounter } from '@@interface/domain';

export type IEntityPremitives = {
  counters: ICounter;
};
