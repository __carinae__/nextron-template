import { EntityAdapter } from '@reduxjs/toolkit';
import * as counter from './counter';

/**
 * Register and integrate related interfaces.
 * */
export type IEntityPremitives = {
  counter: counter.IEntityPremitives;
};

export type IEntityRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: IEntityPremitives[K][T][] };
};

export type IEntityHashRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: { [ids: string]: IEntityPremitives[K][T] } };
};

export type AdapterRecords = {
  [K in keyof IEntityPremitives]: { [T in keyof IEntityPremitives[K]]: EntityAdapter<IEntityPremitives[K][T]> };
};
