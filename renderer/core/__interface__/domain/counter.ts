export type ICounter = {
  id?: string;
  count?: number;
  upperBound?: number;
  lowerBound?: number;
};
