import { EntityState } from '@reduxjs/toolkit';
import { ICounter } from '@@interface/domain';

export type StateSchema = {
  counters: EntityState<ICounter>;
};
