import { EntityState } from '@reduxjs/toolkit';
import * as counter from './counter';

export type StateSchemaFrame = Record<string, EntityState<Partial<Record<string, unknown>>> | unknown>;

/**
 * Register and integrate related interfaces.
 * */
export type StateSchemas = {
  counter: counter.StateSchema;
};

export type SortFilterProps = {
  sortKey?: string[];
  sortOrder?: 'desc' | 'asc';
  filters?: { key: string[]; queries: (string | number | CallableFunction)[] }[];
};

export type SelectorPayload<Entities> = {
  entities: Entities;
} & SortFilterProps;
