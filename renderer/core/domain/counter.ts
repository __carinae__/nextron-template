import { ICounter } from '@@interface';

export const getInitialCounter = (id: string): ICounter => ({
  id,
  count: 0,
  upperBound: null,
  lowerBound: 0,
});

export const increment = (counter: ICounter, diff: number): ICounter => {
  if (counter.upperBound == null) {
    return { count: counter.count + diff };
  } else {
    return { count: Math.min(counter.count + diff, counter.upperBound) };
  }
};

export const decrement = (counter: ICounter, diff: number): ICounter => {
  return { count: Math.max(counter.count - diff, counter.lowerBound) };
};
