import { createEntityAdapter } from '@reduxjs/toolkit';
import { PickType, ICounter, AdapterRecords } from '@@interface';
import { name } from '@@identifier/core/counter';

export const adapterRecord: PickType<AdapterRecords, typeof name> = {
  counters: createEntityAdapter<ICounter>(),
};
