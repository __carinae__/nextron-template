import React from 'react';
import Head from 'next/head';
import { useStyles } from '@@pageComponents/style/home';
import { LoopingRhombusesSpinner, Rhombus, BoundBall } from '@@pageComponents/style/loading/loopingRhombus';
import pack from '@@package';

const Loading: React.FC = () => {
  const classes = useStyles({});

  return (
    <React.Fragment>
      <Head>
        <title>{pack.productName}</title>
      </Head>
      <div className={classes.root}>
        <LoopingRhombusesSpinner>
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <Rhombus />
          <BoundBall />
        </LoopingRhombusesSpinner>
      </div>
    </React.Fragment>
  );
};

export default Loading;
