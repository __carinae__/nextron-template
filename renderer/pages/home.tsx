import { ipcRenderer } from 'electron';
import React from 'react';
import Head from 'next/head';
import { Tab, Paper, Drawer, List, ListItem, Tooltip } from '@material-ui/core';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import { Help } from '@material-ui/icons';
import { useStyles } from '@@pageComponents/style/home';
import { panels, panelKeys } from '@@pageComponents/panels';
import { useDispatch } from 'react-redux';
import { actions, save } from '@@core/redux';
import pack from '@@package';

const Home: React.FC = () => {
  const classes = useStyles({});

  const dispatch = useDispatch();
  const openHelpDialog = () => dispatch(actions.help.setOpen(true));
  const closeHelpDialog = () => dispatch(actions.help.setOpen(false));

  const [pageID, setPageID] = React.useState(panelKeys[0]);
  const changePage = (event: React.ChangeEvent<unknown>, ID: string) => {
    setPageID(ID);
    closeHelpDialog();
  };

  // Save all the current data when catching the signal of terminating the main window.
  ipcRenderer.on('on-app-closing', () => {
    save(dispatch);
    ipcRenderer.send('quitter');
  });

  ipcRenderer.on('save', () => {
    save(dispatch);
  });

  return (
    <React.Fragment>
      <Head>
        <title>{pack.productName}</title>
      </Head>
      <div className={classes.root}>
        <TabContext value={pageID}>
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
            anchor="left">
            <TabList
              selectionFollowsFocus
              value={pageID}
              onChange={changePage}
              indicatorColor="secondary"
              textColor="secondary"
              orientation="vertical"
              className={classes.tabs}
              style={{ height: '100vh' }}
              aria-label="icon label tabs example">
              {panelKeys.map(key => (
                <Tab
                  icon={
                    <Tooltip
                      title={panels[key].name}
                      arrow={pageID !== key}
                      classes={{ tooltip: pageID === key ? classes.tabTooltip : classes.tabTooltipUnselected }}
                      placement="right">
                      {React.createElement(panels[key].icon)}
                    </Tooltip>
                  }
                  key={'panel-tab-' + key}
                  value={key}
                  classes={{ root: classes.tab }}
                  disableRipple
                />
              ))}
            </TabList>
          </Drawer>
          <Paper className={classes.content}>
            {panelKeys.map(key => (
              <TabPanel key={'panel-' + key} value={key}>
                {React.createElement(panels[key].component)}
                {panels[key].help === null ? null : React.createElement(panels[key].help)}
              </TabPanel>
            ))}
          </Paper>
        </TabContext>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaperRight,
          }}
          anchor="right">
          <List>
            <ListItem button style={{ backgroundColor: 'transparent' }} disableRipple onClick={openHelpDialog}>
              <Tooltip title="Help" placement="left" arrow classes={{ tooltip: classes.tooltip }}>
                <Help />
              </Tooltip>
            </ListItem>
          </List>
        </Drawer>
      </div>
    </React.Fragment>
  );
};

export default Home;
