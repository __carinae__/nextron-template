# Nextron Template Repository

This is a template project for developing Nextron single-page desktop application with Clean Architecture concept.
Auto save and load of data with local json files are supported.

## Requirements

- npm: ^16.0.0
- yarn: ^1.22

## Install Dependencies

```bash
$ cd nextron-template
$ yarn install
```

## Fix the codes with ESLint & Prettier

```bash
$ yarn fix:all
```

Code checkers will be also automatically activated when a new git commit is done.

## Usage

```bash
# development mode
$ yarn dev
# you can also specify the port (default port: 8888)
$ yarn dev -p 8889

# production build
$ yarn build
# you can also specify the build options, for example,
$ yarn build:mac
```

## Functionalities

- [x] Clean architecture with typescript interfaces and redux toolkit
- [x] Auto-defined EntityAdapters which have sortable & filterable selectors of entities
- [x] Single page application which can switch arbitrary numbers of content panels
- [x] Data persistency with local json database
- [x] ESLint & Prettier code format checker
- [x] Loading screen in launching application
- [x] Custom application menu
- [x] Data override onto default data so that user-made database could be kept even if the app (=default data) would be updated
- [x] Data override onto outdated data so that critical content changes to already existing enttities can be reflected
- [x] Keyboard shortcuts for data save & other ops
- [x] Save when closing main window (Electron IPC)

## Unsupported functionalities

- [ ] Undo/Redo functions which can work with redux toolkit
  - A certain state change might cause the flow of other state changes. It leads to quite complicated undo/redo implementation to define every kinds of state changes that could be regarded as individual action results.
  - With proper CRUD ops, it is easy to fix the previous user action. Therefore, there is only a slight merit compared to the burden of implementing such functions.
