import { app, ipcMain, Menu } from 'electron';
import serve from 'electron-serve';
import { createWindow, menuTemplate } from './helpers';

const isProd: boolean = process.env.NODE_ENV === 'production';
if (isProd) {
  serve({ directory: 'app' });
} else {
  app.setPath('userData', `${app.getPath('userData')} (dev)`);
}
app.disableHardwareAcceleration();

const loadURL = (screen, name) => {
  if (isProd) {
    screen.loadURL(`app://./${name}.html`);
  } else {
    const port = process.argv[2];
    screen.loadURL(`http://localhost:${port}/${name}`);
  }
};

const windowOps = {
  width: 2100,
  height: 1200,
  show: false,
  // A frameless window is obviously cooler, but it's less user-friendly
  // (especially to those who don't know keyboard shortcuts like ctrl(command)+W)...
  // frame: false
};

let loadingScreen;
const createLoadingScreen = () => {
  // create a browser window
  loadingScreen = createWindow('loading', { ...windowOps, transparent: false });
  loadingScreen.setResizable(false);
  loadURL(loadingScreen, 'loading');
  loadingScreen.on('closed', () => (loadingScreen = null));
  loadingScreen.webContents.on('did-finish-load', () => {
    loadingScreen.show();
  });
};

let preventDefaultMainOnce = true;
const createMainWindow = () => {
  // Window default settings here, but dev screen size also seems to be automatically saved and refered.
  const mainWindow = createWindow('main', { ...windowOps, show: false });

  loadURL(mainWindow, 'home');

  mainWindow.webContents.on('did-finish-load', () => {
    // When the content has loaded, hide the loading screen and show the main window
    let bounds;
    if (loadingScreen === null) {
      bounds = mainWindow.getBounds();
    } else {
      bounds = loadingScreen.getBounds();
      loadingScreen.close();
    }
    mainWindow.show();
    mainWindow.setBounds(bounds);
  });

  mainWindow.on('close', e => {
    if (preventDefaultMainOnce) {
      e.preventDefault();
      mainWindow.webContents.send('on-app-closing');
    }
  });

  ipcMain.on('quitter', () => {
    preventDefaultMainOnce = false;
    mainWindow.close();
  });
};

app.on('ready', () => {
  console.log('Configs and data are stored in ' + app.getPath('userData'));
  createLoadingScreen();

  setTimeout(() => {
    createMainWindow();
  }, 2000);

  const menu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(menu);
});

app.on('window-all-closed', () => {
  app.quit();
});
